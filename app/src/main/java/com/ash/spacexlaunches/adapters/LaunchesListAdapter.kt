package com.ash.spacexlaunches.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ash.spacexlaunches.R
import com.ash.spacexlaunches.data.model.Launch
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.text.SimpleDateFormat

public class LaunchesListAdapter(private val launches: List<Launch>) :
    RecyclerView.Adapter<LaunchesListAdapter.ListHolder>() {

    private val clickSubject = PublishSubject.create<Launch>()


    override fun onBindViewHolder(holder: ListHolder, position: Int) {
        holder.bind(launches[position])
    }

    override fun getItemCount(): Int {
        return launches.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return ListHolder(view)
    }

    val clickEvent: Observable<Launch> = clickSubject

    inner class ListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var id = itemView.findViewById(R.id.id) as TextView
        private var date = itemView.findViewById(R.id.date) as TextView
        private var name = itemView.findViewById(R.id.name) as TextView

        init {
            itemView.setOnClickListener {
                clickSubject.onNext(launches[layoutPosition])
            }
        }

        fun bind(launch: Launch) {
            with(launch) {
                val dateString = with(SimpleDateFormat("dd-MM-yyyy")) { parse(launch_date_local) }
                id.text = "Id : ${mission_id.firstOrNull()}"
                date.text = dateString.toString()
                name.text = mission_name
            }
        }

    }
}