package com.ash.spacexlaunches.utils

import com.ash.spacexlaunches.viewmodels.LaunchesListViewModelFactory

object InjectorUtils {
    fun provideLaunchesListViewModelFactory(): LaunchesListViewModelFactory {
        return LaunchesListViewModelFactory()
    }
}