package com.ash.spacexlaunches.di

import com.ash.spacexlaunches.data.model.Launch
import com.ash.spacexlaunches.data.model.Rocket
import com.ash.spacexlaunches.data.network.RepoService
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit


@Module
@Suppress("unused")
object LaunchModule {


    @Provides
    @Reusable
    @JvmStatic
    internal fun provideLaunch(rocket: Rocket): Launch {
        return Launch("Test Mission", "2006-03-24T22:30:00.000Z", true, arrayOf("121"), rocket)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRocket(): Rocket {
        return Rocket("falconTest1", "Falcon Test", "Falcon F6")
    }



}