package com.ash.spacexlaunches.di

import com.ash.spacexlaunches.ui.RocketDetail
import com.ash.spacexlaunches.viewmodels.LaunchesListViewModel
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Singleton
@Component(modules = [(LaunchModule::class)])
interface LaunchModuleInjector {

    fun inject(rocketDetail: RocketDetail)

    @Component.Builder
    interface Builder {
        fun build(): LaunchModuleInjector

        fun launchModule(launchModule: LaunchModule): Builder
    }

}