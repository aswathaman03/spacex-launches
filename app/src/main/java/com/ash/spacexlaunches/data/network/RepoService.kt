package com.ash.spacexlaunches.data.network

import com.ash.spacexlaunches.data.model.Launch
import io.reactivex.Observable
import retrofit2.http.GET

interface RepoService {

    @GET("v3/launches")
    fun getLaunches(): Observable<List<Launch>>

}