package com.ash.spacexlaunches.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Rocket(val rocket_id: String, val rocket_name: String, val rocket_type: String) : Parcelable