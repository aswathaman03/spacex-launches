package com.ash.spacexlaunches.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Launch(
    val mission_name: String,
    val launch_date_local: String,
    val tbd: Boolean,
    val mission_id: Array<String>,
    val rocket: Rocket
) : Parcelable