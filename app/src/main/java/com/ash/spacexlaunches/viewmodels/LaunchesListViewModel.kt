package com.ash.spacexlaunches.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import com.ash.spacexlaunches.data.model.Launch
import com.ash.spacexlaunches.data.network.RepoService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javax.inject.Inject


class LaunchesListViewModel() : BaseViewModel() {

    @Inject
    lateinit var repoService: RepoService
    private lateinit var subscription: Disposable

    public var filterApplied = MutableLiveData<Boolean>().apply { value = false }

    public var launches = MutableLiveData<List<Launch>>()
    public var launchesLoadError = MutableLiveData<Boolean>()
    public var loading = MutableLiveData<Boolean>()

    init {
        fetchRepos()
    }

    public fun fetchRepos() {
        subscription =
            repoService.getLaunches()
                .concatMap { dbPostList ->
                    Observable.just(dbPostList)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrievePostListStart() }
                .doOnTerminate { onRetrievePostListFinish() }
                .subscribe(
                    { result -> onRetrievePostListSuccess(result) },
                    { error -> onRetrievePostListError() }
                )
    }

    private fun onRetrievePostListStart() {
        loading.value = true
        launchesLoadError.value = false
    }

    private fun onRetrievePostListFinish() {
        loading.value = false
    }

    private fun onRetrievePostListSuccess(launchList: List<Launch>) {
        launches.value = launchList
    }

    private fun onRetrievePostListError() {
        launchesLoadError.value = true
        loading.value = false
    }


    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

}