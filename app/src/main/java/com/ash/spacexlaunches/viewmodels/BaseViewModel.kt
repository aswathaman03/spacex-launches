package com.ash.spacexlaunches.viewmodels

import androidx.lifecycle.ViewModel
import com.ash.spacexlaunches.di.DaggerViewModelInjector
import com.ash.spacexlaunches.di.NetworkModule
import com.ash.spacexlaunches.di.ViewModelInjector

abstract class BaseViewModel : ViewModel() {


    private val injector: ViewModelInjector = DaggerViewModelInjector.builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is LaunchesListViewModel -> injector.inject(this);
        }
    }
}