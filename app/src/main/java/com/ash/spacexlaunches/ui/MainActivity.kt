package com.ash.spacexlaunches.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ash.spacexlaunches.R
import com.ash.spacexlaunches.adapters.LaunchesListAdapter
import com.ash.spacexlaunches.data.model.Launch
import com.ash.spacexlaunches.utils.InjectorUtils
import com.ash.spacexlaunches.viewmodels.LaunchesListViewModel
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.loadingView
import kotlinx.android.synthetic.main.activity_main.recyclerView
import kotlinx.android.synthetic.main.activity_main.tvError

class MainActivity : AppCompatActivity() {


    private var adapter: LaunchesListAdapter? = null
    private var subscribe: Disposable? = null

    private val launchesListViewModel: LaunchesListViewModel by viewModels {
        InjectorUtils.provideLaunchesListViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        observeViewModel()
        filter.setOnCheckedChangeListener { buttonView, isChecked ->
            launchesListViewModel.filterApplied.value = isChecked
        }
        refresh.setOnRefreshListener {
            launchesListViewModel.fetchRepos()
            refresh.isRefreshing = false
            filter.visibility = View.INVISIBLE
            recyclerView.visibility = View.GONE
        }
    }

    fun openLaunchDetail(launch: Launch) {
        val intent = Intent(this, RocketDetail::class.java)
        intent.putExtra("LAUNCH", launch)
        startActivity(intent)
    }

    private fun setupRecyclerView(launches: List<Launch>) {
        filter.visibility = View.VISIBLE
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = LaunchesListAdapter(launches)
        recyclerView.adapter = adapter
        setupItemClick()
    }

    fun observeViewModel() {

        launchesListViewModel.filterApplied.observe(this, Observer { value ->
            launchesListViewModel.launches.let {
                it.value?.let { it1 ->
                    setupRecyclerView(it1.filter {
                        if (value)
                            it.tbd.equals(value)
                        else
                            it.tbd.equals(value) || it.tbd.equals(!value)
                    })
                    recyclerView.visibility = View.VISIBLE
                }
            }
        })

        launchesListViewModel.launches.observe(this, Observer { value ->
            if (value != null && value.size > 0) {
                recyclerView.visibility = View.VISIBLE
                setupRecyclerView(value.filter {
                    val isFilterApplied = launchesListViewModel.filterApplied.value!!
                    if (isFilterApplied)
                        it.tbd.equals(isFilterApplied)
                    else
                        it.tbd.equals(isFilterApplied) || it.tbd.equals(!isFilterApplied)
                })
            } else recyclerView.visibility = View.GONE
        })

        launchesListViewModel.loading.observe(this, Observer { value ->
            if (value) loadingView.visibility = View.VISIBLE
            else loadingView.visibility = View.GONE
        })
        launchesListViewModel.launchesLoadError.observe(this, Observer { value ->
            if (value) tvError.visibility = View.VISIBLE
            else tvError.visibility = View.GONE
        })

    }

    private fun setupItemClick() {
        adapter!!.notifyDataSetChanged()
        subscribe = adapter!!.clickEvent
            .subscribe{
                openLaunchDetail(it)
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        subscribe?.dispose()
    }

}
