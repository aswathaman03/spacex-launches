package com.ash.spacexlaunches.ui

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import com.ash.spacexlaunches.R
import com.ash.spacexlaunches.data.model.Launch
import com.ash.spacexlaunches.di.DaggerLaunchModuleInjector
import com.ash.spacexlaunches.di.DaggerViewModelInjector
import com.ash.spacexlaunches.di.LaunchModule
import com.ash.spacexlaunches.di.NetworkModule

import kotlinx.android.synthetic.main.activity_rocket_detail.*
import javax.inject.Inject

class RocketDetail : AppCompatActivity() {

    init {
        DaggerLaunchModuleInjector.builder()
            .launchModule(LaunchModule)
            .build().inject(this)
    }

    @Inject
    lateinit var launch: Launch

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rocket_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        launch = intent.getParcelableExtra("LAUNCH") as Launch
        updateUi()
    }


    fun updateUi() {
        with(launch) {
            rocketId.text = rocket.rocket_id
            rocketName.text = rocket.rocket_name
            rocketType.text = rocket.rocket_type
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
